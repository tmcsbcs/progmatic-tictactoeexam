package com.progmatic.tictactoeexam.interfaces;

import com.progmatic.tictactoeexam.Cell;
import com.progmatic.tictactoeexam.enums.PlayerType;

/**
 *
 * @author tom
 */
public class SimplePlayer extends AbstractPlayer{

    public SimplePlayer(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {
        if(b.emptyCells().isEmpty()){
            return null;
        }else{
            return b.emptyCells().get(0);   
        }
    }
}
