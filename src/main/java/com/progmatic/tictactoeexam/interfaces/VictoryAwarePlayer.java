package com.progmatic.tictactoeexam.interfaces;

import com.progmatic.tictactoeexam.Cell;
import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tom
 */
public class VictoryAwarePlayer extends AbstractPlayer{
    
    Player player;
    
    public VictoryAwarePlayer(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {
        PlayerType p=player.getMyType();
        Cell c = null;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                try {
                    
                    

                    if(b.getCell(i, j)==p && b.getCell(i+1, j)==p && b.getCell(i+2, j)==PlayerType.EMPTY){
                        return c=new Cell(i+2, j+1, p);
                    }
                    else if(b.getCell(i, j)==p && b.getCell(i+1, j)==PlayerType.EMPTY && b.getCell(i+2, j)==p){
                        return c=new Cell(i+1, j+1, p);
                    }
                    else if(b.getCell(i, j)==PlayerType.EMPTY && b.getCell(i+1, j)==p && b.getCell(i+2, j)==p){
                        return c=new Cell(i, j+1, p);
                    }

                    
                    
                    else if(b.getCell(i, j+1)==p && b.getCell(i+1, j+1)==p && b.getCell(i+2, j+1)==PlayerType.EMPTY){
                        return c=new Cell(i+2, j+1, p);
                    }
                    else if(b.getCell(i, j+1)==p && b.getCell(i+1, j+1)==PlayerType.EMPTY && b.getCell(i+2, j+1)==p){
                        return c=new Cell(i+1, j+1, p);
                    }
                    else if(b.getCell(i, j+1)==PlayerType.EMPTY && b.getCell(i+1, j+1)==p && b.getCell(i+2, j+1)==p){
                        return c=new Cell(i, j+1, p);
                    }
                    
                    
                    
                    else if(b.getCell(i, j+2)==p && b.getCell(i+1, j+2)==p && b.getCell(i+2, j+2)==PlayerType.EMPTY){
                        return c=new Cell(i+2, j+2, p);
                    }
                    else if(b.getCell(i, j+2)==p && b.getCell(i+1, j+2)==PlayerType.EMPTY && b.getCell(i+2, j+2)==p){
                        return c=new Cell(i+1, j+2, p);
                    }
                    else if(b.getCell(i, j+2)==PlayerType.EMPTY && b.getCell(i+1, j+2)==p && b.getCell(i+2, j+2)==p){
                        return c=new Cell(i, j+2, p);
                    }
                    //--------------------------------------------------------------
                    
                    
                    if(b.getCell(i, j)==p && b.getCell(i, j+1)==p && b.getCell(i, j+2)==PlayerType.EMPTY){
                        return c=new Cell(i, j+2, p);
                    }
                    else if(b.getCell(i, j)==p && b.getCell(i, j+1)==PlayerType.EMPTY && b.getCell(i, j+2)==p){
                        return c=new Cell(i, j+1, p);
                    }
                    else if(b.getCell(i, j)==PlayerType.EMPTY && b.getCell(i, j+1)==p && b.getCell(i, j+2)==p){
                        return c=new Cell(i, j, p);
                    }

                    
                    
                    if(b.getCell(i+1, j)==p && b.getCell(i+1, j+1)==p && b.getCell(i+1, j+2)==PlayerType.EMPTY){
                        return c=new Cell(i+1, j+2, p);
                    }
                    else if(b.getCell(i+1, j)==p && b.getCell(i+1, j+1)==PlayerType.EMPTY && b.getCell(i+1, j+2)==p){
                        return c=new Cell(i+1, j+1, p);
                    }
                    else if(b.getCell(i+1, j)==PlayerType.EMPTY && b.getCell(i+1, j+1)==p && b.getCell(i+1, j+2)==p){
                        return c=new Cell(i+1, j, p);
                    }

                    
                    
                    
                    if(b.getCell(i+2, j)==p && b.getCell(i+2, j+1)==p && b.getCell(i+2, j+2)==PlayerType.EMPTY){
                        return c=new Cell(i+2, j+2, p);
                    }
                    else if(b.getCell(i+2, j)==p && b.getCell(i+2, j+1)==PlayerType.EMPTY && b.getCell(i+2, j+2)==p){
                        return c=new Cell(i+2, j+1, p);
                    }
                    else if(b.getCell(i+2, j)==PlayerType.EMPTY && b.getCell(i+2, j+1)==p && b.getCell(i+2, j+2)==p){
                        return c=new Cell(i+2, j, p);
                    }

                    //----------------------------------------------------------
                    
                    if(b.getCell(i, j)==p && b.getCell(i+1, j+1)==p && b.getCell(i+2, j+2)==PlayerType.EMPTY){
                        return c=new Cell(i+2, j+2, p);
                    }
                    else if(b.getCell(i, j)==p && b.getCell(i+1, j+1)==PlayerType.EMPTY && b.getCell(i+2, j+2)==p){
                        return c=new Cell(i+1, j+1, p);
                    }
                    else if(b.getCell(i, j)==PlayerType.EMPTY && b.getCell(i+1, j+1)==p && b.getCell(i+2, j+2)==p){
                        return c=new Cell(i, j, p);
                    }
                    
                    
                    
                    if(b.getCell(i+2, j)==p && b.getCell(i+1, j+1)==p && b.getCell(i, j+2)==PlayerType.EMPTY){
                        return c=new Cell(i, j+2, p);
                    }
                    else if(b.getCell(i+2, j)==p && b.getCell(i+1, j+1)==PlayerType.EMPTY && b.getCell(i, j+2)==p){
                        return c=new Cell(i+1, j+1, p);
                    }
                    else if(b.getCell(i+2, j)==PlayerType.EMPTY && b.getCell(i+1, j+1)==p && b.getCell(i, j+2)==p){
                        return c=new Cell(i+2, j, p);
                    }

                    
                    
                    

                } catch (CellException ex) {
                    CellException ce=new CellException(i, j, "hiba");
                }
            }
        }

        
        
        
        
        
//        ArrayList<Cell>list=new ArrayList<>();
//        list.addAll(b.emptyCells());
//
//        for (int i = 0; i < list.size(); i++) {
//            int rowIdx=list.get(i).getRow();
//            int colIdx=list.get(i).getCol();
//            PlayerType p=list.get(i).getCellsPlayer();
//        }
        return c;
    }
    
}
