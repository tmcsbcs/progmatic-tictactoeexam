package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tom
 */
public class MyBoard implements com.progmatic.tictactoeexam.interfaces.Board {

    public Cell[][] board;

    public MyBoard() {
        board = new Cell[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Cell c = new Cell(i, j);
                board[i][j] = c;
            }
        }
    }

    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {
        PlayerType p;
        try {
            p = board[rowIdx][colIdx].getCellsPlayer();
        } catch (ArrayIndexOutOfBoundsException ae) {
            String msg = "Ilyen indexű elem nem létezik";
            CellException ce = new CellException(rowIdx, colIdx, msg);
            throw ce;
        }
        return p;
    }

    @Override
    public void put(Cell cell) throws CellException {
        int rowIdx = cell.getRow();
        int colIdx = cell.getCol();
        PlayerType p = cell.getCellsPlayer();

        try {
            if (board[rowIdx][colIdx].getCellsPlayer() == PlayerType.EMPTY) {
                board[rowIdx][colIdx] = cell;
            } else {
                throw new CellException(rowIdx, colIdx, "A cella nem üres");
            }

        } catch (ArrayIndexOutOfBoundsException ae) {
            String msg = "Ilyen indexű elem nem létezik";
            CellException ce = new CellException(rowIdx, colIdx, msg);
            throw ce;
        }
    }

    @Override
    public boolean hasWon(PlayerType p) {
        boolean result=false;
//        if (board[1][1].getCellsPlayer()==p) {
//            for (int i = 0; i < 3; i++) {
//                if (board[0][i].getCellsPlayer()==p && board[2][board.length-i].getCellsPlayer()==p) {
//                    result= true;
//                }else if(board[0][i].getCellsPlayer()==p && board[2][board.length-i].getCellsPlayer()==p){
//
//                }
//            }
//        }else{
            if(board[0][0].getCellsPlayer()==p && board[0][1].getCellsPlayer()==p && board[0][2].getCellsPlayer()==p){
                result=true;
            }else if(board[2][0].getCellsPlayer()==p && board[2][1].getCellsPlayer()==p && board[2][2].getCellsPlayer()==p){
                result=true;                
            }else if(board[0][0].getCellsPlayer()==p && board[1][0].getCellsPlayer()==p && board[2][0].getCellsPlayer()==p){
                result=true;                
            }else if(board[0][2].getCellsPlayer()==p && board[1][2].getCellsPlayer()==p && board[2][2].getCellsPlayer()==p){
                result=true;                
            }else if(board[0][0].getCellsPlayer()==p && board[1][1].getCellsPlayer()==p && board[2][2].getCellsPlayer()==p){
                result=true;                
            }else if(board[0][2].getCellsPlayer()==p && board[1][1].getCellsPlayer()==p && board[2][0].getCellsPlayer()==p){
                result=true;                
            }else if(board[1][0].getCellsPlayer()==p && board[1][1].getCellsPlayer()==p && board[1][2].getCellsPlayer()==p){
                result=true;
            }else if(board[0][1].getCellsPlayer()==p && board[1][1].getCellsPlayer()==p && board[2][1].getCellsPlayer()==p){
                result=true;                
            }
//        }   
        return result;
    }

    @Override
    public List<Cell> emptyCells() {
        List<Cell>list=new ArrayList<>();

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Cell c=board[i][j];
                if(c.getCellsPlayer()==PlayerType.EMPTY){
                    list.add(c);
                }
            }
        }
        return list;
    }
}
